package rest

import (
	"fmt"
	"net/http"
	"time"
)

type Status struct {
	GitLab   int
	Database int
	Uptime   string
	Version  string
}

var startTime time.Time

func uptime() string {
	return time.Since(startTime).String()
}

func InitTime() {
	startTime = time.Now()
}

func HandlerStatus(w http.ResponseWriter, r *http.Request) {

  var status Status
	// GitLab:
  url := "https://git.gvk.idi.ntnu.no/api"
  resp, err := http.Get(url)

  if err != nil {
          fmt.Println("Error with GitLab url", err)
  }

  defer resp.Body.Close()

	statusGit := resp.StatusCode

	status.GitLab   = statusGit
	status.Database = 0
	status.Uptime   = uptime()
	status.Version  = "v1"

	format(w, status)
}
