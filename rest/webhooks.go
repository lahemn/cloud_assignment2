package rest

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"cloud.google.com/go/firestore"
	"google.golang.org/api/iterator"
)

var Db FirestoreDatabase

func HandlerWebhooks(w http.ResponseWriter, r *http.Request) {

	switch r.Method {

	case http.MethodGet:

		webHookId := ""
		webHookId = r.URL.Path[23:]
		var iteration *firestore.DocumentIterator
		var webhook Webhook
		var listWebhook []Webhook

		if webHookId != "" {
			iteration = Db.Client.Collection("webhooks").Where("ID", "==", webHookId).Documents(Db.Ctx)
		} else {
			iteration = Db.Client.Collection("webhooks").Documents(Db.Ctx)
		}

		for { //loops through document, adds to array if ok
			doc, err := iteration.Next()
			if err == iterator.Done {
				break
			}
			if err != nil {
				log.Fatalf("Failed to iterate: %v", err)
			}
			if doc != nil {
				err = doc.DataTo(&webhook)
				listWebhook = append(listWebhook, webhook)
			}
		}

		w.Header().Add("content-type", "application/json")
		err := json.NewEncoder(w).Encode(listWebhook)
		if err != nil {
			http.Error(w, "failed to encode webhoks", http.StatusInternalServerError)
		}

	case http.MethodDelete:
		var deleteHook Webhook

		webhookIdentifier := r.URL.Path[23:]
		deleteHook.ID = webhookIdentifier //finds webhook to delete

		err := Db.Delete(&deleteHook)
		if err != nil {
			log.Fatal(err)
		}

	case http.MethodPost:

		var postHook Webhook

		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&postHook)
		if err != nil {
			http.Error(w, "Could not decode", http.StatusBadRequest)

		}

		err = Db.Save(&postHook) //Saves
		if err != nil {
			log.Fatal(err)
		}

	default:

		fmt.Fprintf(w, "not implemented%v")
	}
}

func webHooksInvoke(event string, params []string, currentTime time.Time) {
	iter := Db.Client.Collection("webhooks").Documents(Db.Ctx)
	for { //goes through all webhooks
		var webHook Webhook
		doc, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Printf("Failed to iterate: %v", err)
		}
		err = doc.DataTo(&webHook)
		if err != nil {
			log.Printf("Failed to get response: %v", err)
		}
		if webHook.Event == event {
			requestWebhook := Invocation{Event: event, Params: params, Time: time.Now()}
			requestBody, err := json.Marshal(requestWebhook)
			if err != nil {
				log.Printf("Failed %v", err)
			}
			_, err = http.Post(webHook.URL, "application/json", bytes.NewBuffer(requestBody))
			if err != nil {
				log.Printf("Failed", err)
			}
		}
	}
}
