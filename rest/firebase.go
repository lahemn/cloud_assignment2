package rest

import (
	"context"
	"fmt"
	"log"

	"cloud.google.com/go/firestore"
	"google.golang.org/api/option"
)

func (db *FirestoreDatabase) Init() error {
	db.Ctx = context.Background()
	var err error
	//Needs credentials file from firebase project
	sa := option.WithCredentialsFile("./sustained-racer-257906-firebase-adminsdk-rl8wu-2b4d9934c9.json")
	db.Client, err = firestore.NewClient(db.Ctx, db.ProjectID, sa)
	if err != nil {
		fmt.Printf("Error in FirebaseDatabase.Init() function: %v\n", err)
		log.Fatal(err, "Error in FirebaseDatabase.Save()")
	}
	return nil
}

func (db *FirestoreDatabase) Close() {
	_ = db.Client.Close()
}

func (db *FirestoreDatabase) Save(s *Webhook) error {
	ref := db.Client.Collection(db.CollectionName).NewDoc()
	s.ID = ref.ID
	_, err := ref.Set(db.Ctx, s)
	if err != nil {
		fmt.Println("ERROR saving student to Firestore DB: ", err)
		log.Fatal(err, "Error in FirebaseDatabase.Save()")
	}
	return nil
}

func (db *FirestoreDatabase) Delete(s *Webhook) error {
	docRef := db.Client.Collection(db.CollectionName).Doc(s.ID)
	_, err := docRef.Delete(db.Ctx)
	if err != nil {
		fmt.Printf("ERROR deletingfrom Firestore DB: %v\n", s, err)
		log.Fatal(err, "Error in FirebaseDatabase.Save()")
	}
	return nil
}
