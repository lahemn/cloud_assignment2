package rest

import (
	"context"
	"time"

	"cloud.google.com/go/firestore"
)

type Project struct {
  Id                  int          `json:"id"`
  Path_with_namespace string       `json:"path_with_namespace"`
}

type Webhook struct {
	ID    string `json:"id"`
	Event string `json:"event"`
	URL   string `json:"url"`
	Time  string `json:"time"`
}

type Invocation struct {
	Event  string    `json:"event"`
	Params []string  `json:"params"`
	Time   time.Time `json:"time"`
}

type FirestoreDatabase struct {
	ProjectID      string
	CollectionName string
	Ctx            context.Context
	Client         *firestore.Client
}

type Database interface {
	Init() error
	Close()
	Save(*Webhook) error
	Delete(*Webhook) error
	ReadByID(string) (Webhook, error)
}
