package rest

import (
  "net/http"
  "encoding/json"
  "fmt"
  "io"
  "strconv"
)

func getAttributes(w http.ResponseWriter, r *http.Request) (int, string) {

  attri    := r.URL.Query()            // Retrieves all attributes

  limit, err := strconv.Atoi(attri.Get("limit"))
  if err != nil {
    http.Error(w, "Must pass in an integer type for limit", http.StatusBadRequest)
    limit = 5
  }
  fmt.Println(limit)

  auth      := attri.Get("auth")
  if (auth == "") {
    auth = "false"
  }

  return limit, auth
}

func getAllProjects (w http.ResponseWriter, url string) []Project {

  var prjPage []Project
  project = []Project{}

  url_prj  := url + "1"

  checkDecoding(w, json.NewDecoder(recieveApi(w, url_prj)).Decode(&prjPage))

  for i := 1; len(prjPage) > 0; i++ {
    fmt.Println(strconv.Itoa(i) + ": Number'" + strconv.Itoa(len(prjPage)) + "'\n")
    url_prj = url + strconv.Itoa(i)

    checkDecoding(w, json.NewDecoder(recieveApi(w, url_prj)).Decode(&prjPage))

    for j := 0; j < len(prjPage); j++ {
      project = append(project, prjPage[j])
    }
  }

  return project

}

func recieveApi(w http.ResponseWriter, url string) io.Reader {
  resp, err := http.Get(url)
  if err != nil {
    http.Error(w, "Could not request server", http.StatusNotFound)
  }
  return resp.Body
}

func checkDecoding(w http.ResponseWriter, err error) {
    if err != nil {
      http.Error(w, "Could not decode the response from client", http.StatusBadRequest)
    }
}

func format(w http.ResponseWriter, v interface{}) {
  json_co, err := json.MarshalIndent(v, "", "    ")
  if err != nil {
    http.Error(w, "Could not encode the object", http.StatusBadRequest)
  }
  fmt.Fprint(w, string(json_co))
}
