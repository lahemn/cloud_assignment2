package rest

import (
  "net/http"
  "encoding/json"
  "fmt"
  "strconv"
)

type LanguageType struct {
  Language     []string
  Auth         bool
}

var language    LanguageType
var url_lang    string
var sort        []string

func HandlerLanguages (w http.ResponseWriter, r *http.Request) {

  langTypes := make(map[string]float64)
  langNumb := make(map[string]int)
  limit, auth := getAttributes(w, r)

  sort = []string{}

  for i := 0; i < limit; i++ {
    sort = append(sort, "")
  }

    // retrieves the url we want to use
  path    := "https://git.gvk.idi.ntnu.no/api/v4/projects"
  query   := "?private_token=" + auth + "&visibility=public&per_page=100&page="

  url_lang = path + query
  project = getAllProjects(w, url_lang)

  for i := 0; i < len(project); i++ {
    fmt.Println(strconv.Itoa(i+1) + "/" + strconv.Itoa(len(project)) + "\n")
    path = "https://git.gvk.idi.ntnu.no/api/v4/projects/" + strconv.Itoa(project[i].Id) + "/languages"

    url_lang = path

    checkDecoding(w, json.NewDecoder(recieveApi(w, url_lang)).Decode(&langTypes))

    for k, _ := range langTypes {
      if langNumb[k] < 1 {
        langNumb[k] = 0
      }
      langNumb[k] += 1
    }

  }

  for key, value := range langNumb {

    emptySlots = 0
    for _, s := range sort {
      if s == "" {
        emptySlots += 1
      }
    }

    if sort[limit-1] == "" {
      sort[limit-1] = key
    }
    if value > langNumb[sort[limit-1]] || emptySlots > 0 {
      for place := limit-1; place > 0; place-- {
        if sort[place-1] != "" {
          sort[place] = sort[place-1]
        }
        if value < langNumb[sort[place-1]] {
          if emptySlots > 0 {
            for move := emptySlots; move < place; move++ {
              sort[move-1] = sort[move]
            }
          }
          sort[place] = key
          break
        }
        if place == 1 {
          sort[place-1] = key
        }
      }
    }
  }

  for i := 0; i < limit; i++ {
    fmt.Println(sort[i] + ": " + strconv.Itoa(langNumb[sort[i]]))
    language.Language = append(language.Language, sort[i])
  }
  if auth == "false" {
    language.Auth = false
  } else {
    language.Auth = true
  }

  format(w, language)

}
