package rest

import (
  "net/http"
  "encoding/json"
  "fmt"
  "strconv"
)

type Commit struct {
  Id           string       `json:"id"`
}

type Repository struct {
  Repository   string
  Commit       int
}

var comPage    []Commit
var commit     int
var repo       []Repository

func HandlerCommits (w http.ResponseWriter, r *http.Request) {

  limit, auth := getAttributes(w, r)

  repo = []Repository{}

  for i := 0; i < limit; i++ {
    repo = append(repo, Repository { Commit: 0, })
  }

    // retrieves the url we want to use
  path    := "https://git.gvk.idi.ntnu.no/api/v4/projects"
  query   := "?private_token=" + auth + "&visibility=public&per_page=100&page="

  url_root := path + query
  project = getAllProjects(w, url_root)

  query = "?all=true&visibility=public&per_page=100&page="

  for i := 0; i < len(project); i++ {
    fmt.Println(strconv.Itoa(i+1) + "/" + strconv.Itoa(len(project)) + "\n")
    commit = 0
    path = "https://git.gvk.idi.ntnu.no/api/v4/projects/" + strconv.Itoa(project[i].Id) + "/repository/commits"

    url_root    = path + query
    url_commit := url_root + "1"

    checkDecoding(w, json.NewDecoder(recieveApi(w, url_commit)).Decode(&comPage))

    for j := 1; len(comPage) > 0; j++ {
      url_commit = url_root + strconv.Itoa(j)

      checkDecoding(w, json.NewDecoder(recieveApi(w, url_commit)).Decode(&comPage))
      commit += len(comPage)

    }

    emptySlots = 0
    for _, r := range repo {
      if r.Commit == 0 {
        emptySlots += 1
      }
    }
    fmt.Println(emptySlots)


    if commit > repo[limit-1].Commit || emptySlots > 0 {
      for place := limit-1; place > 0; place-- {
        if repo[place-1].Commit > 0 {
          repo[place] = repo[place-1]
        }
        if commit < repo[place].Commit {
          if emptySlots > 0 {
            for move := emptySlots; move < place; move++ {
              repo[move-1] = repo[move]
            }
          }
          repo[place] = Repository {
            Repository: project[i].Path_with_namespace,
            Commit:     commit,
          }
          break
        }
        if place == 1 {
          repo[place-1] = Repository {
            Repository: project[i].Path_with_namespace,
            Commit:     commit,
          }
        }
      }
    }

  }
  format(w, repo[:])
}
