module rest

go 1.13

require (
	cloud.google.com/go/firestore v1.0.0
	firebase.google.com/go v3.10.0+incompatible // indirect
	google.golang.org/api v0.13.0
)
