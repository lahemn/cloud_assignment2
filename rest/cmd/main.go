package main

import (
    "fmt"
    "log"
    "net/http"
    "os"
    "rest"
)

func main() {

  rest.InitTime()
  const projectID = "sustained-racer-257906"
  const collection = "webhooks"

  //Initializes database
  rest.Db = rest.FirestoreDatabase{ProjectID: projectID, CollectionName: collection}
  err := rest.Db.Init()
  if err != nil {
    log.Fatal(err)
  }

  defer rest.Db.Close()

  port := os.Getenv("PORT")
  if port == "" {

      port = "8080"
  }

  http.HandleFunc("/repocheck/v1/commits", rest.HandlerCommits)
  http.HandleFunc("/repocheck/v1/languages", rest.HandlerLanguages)
  http.HandleFunc("/repocheck/v1/status", rest.HandlerStatus)
  http.HandleFunc("/repocheck/v1/webhooks/", rest.HandlerWebhooks)

  fmt.Println("Listening on port " + port)
  log.Fatal(http.ListenAndServe(":" + port, nil))

}
